import React, { Component } from 'react';

import AssignmentStatistics from '../../../components/admin/assignmentStatistics/AssignmentStatistics';

class ResultAssignmentStatistics extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {
    // TODO change page title to dynamic title
    this.props.updateTitleType(true);
    this.props.updateTitle('Resultaten');
  }

  render() {
    return <AssignmentStatistics />;
  }
}

export default ResultAssignmentStatistics;
