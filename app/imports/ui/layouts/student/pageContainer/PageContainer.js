import React from 'react';

import './page-container.scss';

const PageContainer = props => <div className="student-page-container">{props.children}</div>;

export default PageContainer;
