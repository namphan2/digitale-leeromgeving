import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';

import ResultTable from '../../../components/admin/resultTable/ResultTable';
import FilterBar from '../../../components/general/filterBar/FilterBar';
import './style.scss';

class ResultOverview extends Component {
  constructor(props) {
    super(props);

    this.sort = this.sort.bind(this);

    this.state = {
      category: [
        {
          name: 'ICT',
          value: 'ICT',
        },
        {
          name: 'Aardrijkskunde',
          value: 'Aardrijkskunde',
        },
      ],
      percentage: [
        {
          name: 'Oplopend',
          value: 1,
        },
        {
          name: 'Aflopend',
          value: -1,
        },
      ],
      name: [
        {
          name: 'A-Z',
          value: 1,
        },
        {
          name: 'Z-A',
          value: -1,
        },
      ],
    };
  }

  componentDidMount() {
    this.props.updateTitleType(true);
    this.props.updateTitle('Resultaten');
  }

  sort(event) {
    this.setState({
      order: parseInt(event.target.value),
      orderBy: event.target.name,
    });
  }

  render() {
    return (
      <div className="statistics-overview-container">
        <div className="top-container">
          <FilterBar
            category={this.state.category}
            percentage={this.state.percentage}
            name={this.state.name}
            onChange={this.sort}
          />
        </div>
        <ResultTable sort={{ orderBy: this.state.orderBy, order: this.state.order }} />
      </div>
    );
  }
}

export default ResultOverview;
