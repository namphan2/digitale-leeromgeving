import React from 'react';
import './header.scss';

const Header = props => (
  <header className="admin-header">
    <div className="title-and-avatar-container">
      {props.isTitle ? (
        <h1 className="page-title">{props.title}</h1>
      ) : (
        <input
          className="page-title-input"
          type="text"
          placeholder="Voer hier uw titel in"
          value={props.title}
          onChange={props.handleTitleUpdate}
        />
      )}

      {/* avatar module */}
    </div>
  </header>
);

export default Header;
