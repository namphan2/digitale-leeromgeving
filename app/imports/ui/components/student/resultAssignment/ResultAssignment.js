import React from 'react';

import './style.scss';

const ResultAssignment = () => {
  const testData = [
    {
      questioncount: 'Vraag 1',
      question: 'Welk van de volgende getallen is GEEN priemgetal?',
      youranswer: 'C. 69',
      correctanswer: 'C. 69',
    },
    {
      questioncount: 'Vraag 2',
      question: 'Welk van de volgende getallen is GEEN priemgetal?',
      youranswer: 'C. 69',
      correctanswer: 'B. 3',
    },
    {
      questioncount: 'Vraag 3',
      question: 'Welk van de volgende getallen is GEEN priemgetal?',
      youranswer: 'C. 69',
      correctanswer: 'C. 69',
    },
    {
      questioncount: 'Vraag 4',
      question: 'Welk van de volgende getallen is GEEN priemgetal?',
      youranswer: 'C. 69',
      correctanswer: 'C. 69',
    },
  ];

  const renderBorderColor = (yourAnswer, correctAnswer) => {
    let name = ['question-block'];

    if (yourAnswer === correctAnswer) {
      name.push(' correct-answer');
    } else {
      name.push(' wrong-answer');
    }

    return name.join('');
  };

  // For each loop for looping through testdata and put it in a variable
  const renderQuestions = testData.map(data => (
    <div
      key={data.questioncount}
      className={renderBorderColor(data.youranswer, data.correctanswer)}
    >
      <p>{data.questioncount}</p>
      <p>{data.question}</p>
      <p>Jouw antwoord</p>
      <p>{data.youranswer}</p>
      <p>Juiste antwoord</p>
      <p>{data.correctanswer}</p>
    </div>
  ));

  return (
    <div className="result-assignment-container">
      {renderQuestions}
    </div>
  );
};

export default ResultAssignment;