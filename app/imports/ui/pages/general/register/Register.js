import React, { Component } from 'react';
import { Mongo } from 'meteor/mongo';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import validate from 'validate.js';
import constraints from '../../../../util/validate/register/constraints';

import './register.scss';
import Alert from '../../../components/general/alert/Alert';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      email: '',
      firstname: '',
      lastname: '',
      password: '',
      passwordRepeat: '',
      teacherCode: '',
      alert: false,
      alertMessage: '',
      alertType: '',
    };
    this.inputHandler = this.inputHandler.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onFormSubmit(e) {
    e.preventDefault();
    const { username, email, password } = this.state;

    const formError = this.validateFields();

    if (!formError) {
      const profile = this.createProfile();

      Accounts.createUser(
        {
          username,
          email,
          password,
          profile,
        },
        (err) => {
          // TODO translate error messages

          if (err) {
            this.setState({
              alert: true,
              alertMessage: err.reason,
              alertType: 'danger',
            });
          } else {
            this.setState({
              alertType: 'success',
              alert: true,
              alertMessage: 'Account is aangemaakt! (Je wordt over 6 seconden doorgestuurd)',
            });

            setTimeout(() => {
              this.props.history.push('/');
            }, 6000);
          }
        },
      );
    }
  }

  inputHandler(event) {
    // Get input all input values from the form
    const inputname = event.target.name;
    const inputvalue = event.target.value;

    this.setState({
      [inputname]: inputvalue,
    });
  }

  validateFields() {
    validate.options = {
      fullMessages: false,
    };

    const {
      username,
      email,
      firstname,
      lastname,
      password,
      passwordRepeat,
      teacherCode,
    } = this.state;

    const validateResult = validate(
      {
        username,
        email,
        firstname,
        lastname,
        password,
        passwordRepeat,
        teacherCode,
      },
      constraints,
    );

    if (validateResult) {
      const message = validateResult[Object.keys(validateResult)[0]][0];

      this.setState({
        alert: true,
        alertMessage: message,
        alertType: 'danger',
      });

      return true;
    }

    this.setState({
      alert: false,
      alertMessage: '',
      alertType: '',
    });

    return false;
  }

  createProfile() {
    const { firstname, lastname, teacherCode } = this.state;

    return {
      firstname,
      lastname,
      teacherCode,
    };
  }

  render() {
    return (
      <div>
        <Alert
          message={this.state.alertMessage}
          type={this.state.alertType}
          isShowAlert={this.state.alert}
        />
        <div className="account-container">
          <h1 className="account-header">Heb je al een account?</h1>
          <div className="account-container-text">
            <form method="get" action="/">
              <input type="submit" className="to-login" value="Naar de inlogpagina" />
            </form>
          </div>
        </div>

        <div className="register-title-container">
          <h1 className="register-header">Registreren</h1>
          <div className="register-container">
            <p className="register-text">Gebruikersnaam</p>
            <input
              type="text"
              name="username"
              className="register-input"
              id="usernameInput"
              value={this.state.username}
              onChange={this.inputHandler}
            />

            <p className="register-text">Emailadres</p>
            <input
              type="email"
              name="email"
              className="register-input"
              id="emailInput"
              value={this.state.email}
              onChange={this.inputHandler}
            />

            <p className="register-text">Voornaam</p>
            <input
              type="text"
              name="firstname"
              className="register-input"
              id="firstnameInput"
              value={this.state.firstname}
              onChange={this.inputHandler}
            />

            <p className="register-text">Achternaam</p>
            <input
              type="text"
              name="lastname"
              className="register-input"
              id="lastnameInput"
              value={this.state.lastname}
              onChange={this.inputHandler}
            />

            <p className="register-text">Wachtwoord</p>
            <input
              type="password"
              name="password"
              className="register-input"
              id="passwordInput"
              value={this.state.password}
              onChange={this.inputHandler}
            />

            <p className="register-text">Herhaal wachtwoord</p>
            <input
              type="password"
              name="passwordRepeat"
              className="register-input"
              id="passwordRepeatInput"
              value={this.state.passwordRepeat}
              onChange={this.inputHandler}
            />

            <p className="register-text">Docentcode</p>
            <input
              type="text"
              name="teacherCode"
              className="register-input"
              id="teachercodeInput"
              value={this.state.teacherCode}
              onChange={this.inputHandler}
            />

            <input
              type="submit"
              className="register-button"
              value="Registreer"
              onClick={this.onFormSubmit}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Register;
