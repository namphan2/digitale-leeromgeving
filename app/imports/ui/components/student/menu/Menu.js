import React from 'react';
import { Link } from 'react-router-dom';

import './menu.scss';

const Menu = () => (
  <aside className="student-sidebar">
    <div className="menu">
      <nav className="main-menu">
        <ul>
          <li className="menu-item">
            <Link to="/student">Mijn opdrachten</Link>
          </li>
          <li className="menu-item">
            <Link to="/student">Mijn toetsen</Link>
          </li>
          <li className="menu-item">
            <Link to="/student/resultaten">Mijn resultaten</Link>
          </li>
          <li className="menu-item">
            <Link to="/student">Mijn cursussen</Link>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
);

export default Menu;
