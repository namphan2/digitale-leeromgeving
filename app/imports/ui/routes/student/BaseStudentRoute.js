import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Questions from '../../components/student/questions/Questions';

import BaseStudentMain from './BaseStudentMain';

const Student = () => (
  <div className="student-area">
    <Switch>
      <Route path="/student/opdrachten/maken" component={Questions} />
      <Route path="/student" component={BaseStudentMain} />
    </Switch>
  </div>
);

export default Student;
