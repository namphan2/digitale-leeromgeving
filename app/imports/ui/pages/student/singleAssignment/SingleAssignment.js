import React, { Component } from 'react';

import ResultAssignment from '../../../components/student/resultAssignment/ResultAssignment';

class SingleAssignment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      subjectname: 'Rekenen oefentoets',
    };
  }

  render() {
    return (
      <div className="student-result-assignment">
        <div className="top-container">{this.state.subjectname}</div>
        <ResultAssignment />
      </div>
    );
  }
}

export default SingleAssignment;
