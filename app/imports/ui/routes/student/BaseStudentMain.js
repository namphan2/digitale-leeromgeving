import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Menu from '../../components/student/menu/Menu';
import PageContainer from '../../layouts/student/pageContainer/PageContainer';
import AssignmentOverview from '../../pages/student/assignmentOverview/AssignmentOverview';
import ResultOverview from '../../pages/student/resultOverview/ResultOverview';
import SingleAssignment from '../../pages/student/singleAssignment/SingleAssignment';

const Student = () => (
  <div className="student-area">
    <div className="student-container">
      <Menu />
      <PageContainer>
        <Switch>
          <Route exact path="/student" component={AssignmentOverview} />
          <Route exact path="/student/resultaten" component={ResultOverview} />
          <Route exact path="/student/resultaten/opdracht" component={SingleAssignment} />
        </Switch>
      </PageContainer>
    </div>
  </div>
);

export default Student;
