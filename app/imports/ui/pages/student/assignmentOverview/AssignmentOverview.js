import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SearchBar from '../../../components/student/searchBar/SearchBar';
import FilterBar from '../../../components/general/filterBar/FilterBar';
import AssignmentTable from '../../../components/student/assignmentTable/AssignmentTable';

import './style.scss';

class AssignmentOverview extends Component {
  constructor(props) {
    super(props);

    this.eventHandler = this.eventHandler.bind(this);
    this.sort = this.sort.bind(this);

    this.state = {
      difficulty: [
        {
          name: 'Makkelijk',
          level: 1,
        },
        {
          name: 'Normaal',
          level: 2,
        },
        {
          name: 'Moeilijk',
          level: 3,
        },
      ],
      category: [
        {
          name: 'Ict',
          value: 'ict',
        },
        {
          name: 'Aardrijkskunde',
          value: 'aardrijkskunde',
        },
      ],
      course: [
        {
          name: 'cursus 1',
        },
        {
          name: 'cursus 2',
        },
      ],
      name: [
        {
          name: 'A-Z',
          value: 1,
        },
        {
          name: 'Z-A',
          value: -1,
        },
      ],
      search: '',
    };
    //   console.log(this.state.name);
    console.log(this.state.name);
  }

  eventHandler(e) {
    this.setState({
      search: e.target.value,
    });
  }

  sort(event) {
    this.setState({
      order: parseInt(event.target.value),
      orderBy: event.target.name,
    });
    // console.log(event.target.value);
  }

  render() {
    return (
      <div className="assignment-overview">
        <SearchBar eventHandler={this.eventHandler} value={this.state.search} />
        <div className="assignment-overview-container">
          <FilterBar
            difficulty={this.state.difficulty}
            category={this.state.category}
            course={this.state.course}
            name={this.state.name}
            onChange={this.sort}
          />
          <AssignmentTable sort={{ orderBy: this.state.orderBy, order: this.state.order }} />
        </div>
      </div>
    );
  }
}

AssignmentOverview.propTypes = {
  search: PropTypes.string,
};

export default AssignmentOverview;
