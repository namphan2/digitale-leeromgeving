import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';

import assignments from '../../../../api/collections/assignments';
import QuickLink from '../../../components/admin/quickLink/QuickLink';
import DashboardTable from '../../../components/admin/dashboardTable/DashboardTable';
import './style.scss';

class Dashboard extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.updateTitleType(true);
    this.props.updateTitle('Dashboard');
  }

  render() {
    return (
      <div className="dashboard-container">
        <div className="quick-link-container">
          <QuickLink number="10" text="test" />
          <QuickLink number="10" text="test" />
          <QuickLink number="10" text="test" />
          <QuickLink number="10" text="test" />
        </div>
        <DashboardTable dataList={this.props.assignments} />
      </div>
    );
  }
}

export default withTracker(() => {
  Meteor.subscribe('assignments.get');

  return {
    assignments: assignments.find({}).fetch(),
  };
})(Dashboard);
