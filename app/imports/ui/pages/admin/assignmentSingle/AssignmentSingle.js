import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import shortid from 'shortid';
import ReactPlayer from 'react-player';
import { toast } from 'react-toastify';

import './Style.scss';
import assignments from '../../../../api/collections/assignments';
import validate from './validate';
import QuestionBlock from '../../../components/admin/questionBlock/QuestionBlock';
import UploadManager from '../../../components/admin/uploadManager/UploadManager';
import VideoBox from '../../../components/admin/videoBox/VideoBox';

class AssignmentSingle extends Component {
  constructor(props) {
    super(props);

    const isUpdate = !!this.props.match.params.id;

    this.state = {
      title: '',
      oldTitle: '',
      description: '',
      status: false,
      alert: {
        message: '',
        type: toast.TYPE.INFO,
        status: false,
      },
      isUpdate,
      questions: [],
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.closeAlert = this.closeAlert.bind(this);
    this.deleteCourse = this.deleteCourse.bind(this);
    this.getQuestionsData = this.getQuestionsData.bind(this);
  }

  componentDidMount() {
    const id = this.props.match.params.id;

    if (id) {
      Meteor.call('assignment.getById', id, (error, result) => {
        console.log(result);
        const {
          name, description, status, visibilty, questions, questionBlocks,
        } = result;
        this.setState({
          title: name,
          oldTitle: name,
          description,
          status,
          visibilty,
          questions,
          questionBlocks,
        });

        this.props.updateTitle(name);
        this.props.updateTitleType(false);
      });
    } else {
      this.props.updateTitle('');
      this.props.updateTitleType(false);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.location.state) {
      const { isUpdate } = this.props.location.state;

      if (isUpdate) {
        this.setState({
          isUpdate,
        });
      }

      this.props.location.state = {};
    }

    if (prevState.title !== prevProps.getTitle()) {
      this.setState({
        title: prevProps.getTitle(),
      });
    }

    if (prevState.alert !== this.state.alert && this.state.alert.status !== false) {
      console.log(this.state.alert);
      const { message, type } = this.state.alert;
      toast(message, { type });
    }
  }

  handleInput(e) {
    const { value, name } = e.target;

    this.setState({
      [name]: value,
    });
  }

  handleSubmit(e) {
    e.preventDefault();

    const formError = validate(this.state, this.props.assignments);

    this.setState(formError);

    if (formError.alert.status) {
      return;
    }

    // https://validatejs.org/#utilities-capitalize

    const { title, description, questions } = this.state;

    const status = e.target.name === 'publish';

    const assignmentObject = {
      name: title,
      description,
      status,
      questions,
    };

    if (this.state.isUpdate) {
      Meteor.call('assignment.update', this.props.match.params.id, assignmentObject, (error) => {
        if (!error) {
          this.setState({
            alert: {
              message: 'Opdracht is geupdate',
              type: toast.TYPE.SUCCESS,
              status: true,
            },
          });
        }
      });
    } else {
      Meteor.call('assignments.add', assignmentObject, (error, result) => {
        if (error) {
          this.setState({
            alert: {
              message: 'Er is iets fout gegaan',
              type: toast.TYPE.ERROR,
              status: true,
            },
          });
        } else {
          this.setState({
            alert: {
              message: 'Opdracht is toegevoegd!',
              type: toast.TYPE.SUCCESS,
              status: true,
            },
          });

          this.props.history.push({
            pathname: `/admin/opdrachten/${result}`,
            state: { isUpdate: true },
          });
        }
      });
    }
  }

  closeAlert() {
    this.setState({
      isAlert: false,
      alertType: '',
      alertMessage: '',
    });
  }

  deleteCourse() {
    if (!this.state.isUpdate) {
      this.setState({
        alert: {
          message: 'Opdracht is nog niet gepubliceerd!',
          type: toast.TYPE.WARNING,
          status: true,
        },
      });

      return;
    }

    if (confirm('Weet u het zeker?')) {
      Meteor.call('assignment.delete', this.props.match.params.id, (error) => {
        if (error) {
          this.setState({
            alert: {
              message: 'Er is iets misgegaan',
              type: toast.TYPE.ERROR,
              status: true,
            },
          });
        } else {
          this.setState({
            alert: {
              message: 'Opdracht is verwijderd (U wordt binnen 3 seconden doorverwezen)',
              type: toast.TYPE.SUCCESS,
              status: true,
            },
          });

          setTimeout(() => {
            this.props.history.push('/admin/opdrachten');
          }, 3000);
        }
      });
    }
  }

  getQuestionsData(data) {
    const { questions } = data;

    this.setState({
      questions,
    });
  }

  render() {
    return (
      <div className="add-questions">
        <div className="page-container">
          <QuestionBlock
            data={{ questions: this.state.questions }}
            onChange={this.getQuestionsData}
          />
        </div>
        <div className="settings-and-delete-container">
          <div className="settings-container padding-content">
            <h3>Instellingen</h3>
            <p>
              <span className="bold">Status:</span>
              {this.state.status ? 'Gepubliceerd' : 'Niet gepubliceerd'}
              <button className="settings-button">x</button>
            </p>
            <p>
              <span className="bold">Groepen:</span> Alle{' '}
              <button className="settings-button">x</button>
            </p>
            <button name="publish" className="Upload" onClick={this.handleSubmit}>
              {this.state.isUpdate ? 'Update' : 'Publiceren'}
            </button>
            <button name="concept" className="SaveConcept" onClick={this.handleSubmit}>
              Opslaan als concept
            </button>
          </div>
          <div className="description-container padding-content">
            <h3>Beschrijving</h3>
            <textarea
              className="description-input"
              type="text"
              onChange={this.handleInput}
              name="description"
              value={this.state.description}
              rows="10"
              cols="50"
            />
          </div>
          <div className="delete-container padding-content">
            <h3>Verwijderen</h3>
            <p>Groep verwijderen?</p>
            <button className="DeleteButton" onClick={this.deleteCourse}>
              Verwijderen
            </button>
          </div>
        </div>
        <UploadManager show={this.state.showMediaModal} onClose={this.closeUploadModal} />
      </div>
    );
  }
}

export default withTracker(() => {
  Meteor.subscribe('assignments.get');

  return {
    assignments: assignments.find({}).fetch(),
  };
})(AssignmentSingle);
