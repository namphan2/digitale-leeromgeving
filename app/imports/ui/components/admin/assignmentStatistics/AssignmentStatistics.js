import React from 'react';

import './style.scss';

const AssignmentStatistics = () => {
  this.state = {
    subjectname: 'Rekenen toets 5',
    totalsubmits: 20,
    result: 71,
  };

  const testData = [
    {
      questioncount: 'Vraag 1',
      question: 'Hoeveel is 33 + 34?',
      correctpercentage: 69,
      wrongpercentage: 31,
    },
    {
      questioncount: 'Vraag 2',
      question: 'Hoeveel is 33 + 34?',
      correctpercentage: 52,
      wrongpercentage: 48,
    },
    {
      questioncount: 'Vraag 3',
      question: 'Hoeveel is 33 + 34?',
      correctpercentage: 10,
      wrongpercentage: 90,
    },
    {
      questioncount: 'Vraag 4',
      question: 'Hoeveel is 33 + 34?',
      correctpercentage: 66,
      wrongpercentage: 34,
    },
  ];

  // For each loop for looping through testdata and put it in a variable
  const renderQuestions = testData.map(data => (
    <div key={data.questioncount} className="question-outside-container">
      <p>{data.questioncount}</p>
      <p className="question-sentence">{data.question}</p>
      <div className="bar-chart-container">
        <div className="bar-in-chart green-bar" style={{ height: data.correctpercentage }} >
          <span className="percentage-info">{data.correctpercentage}%</span>
        </div>
        <div className="bar-in-chart red-bar" style={{ height: data.wrongpercentage }} >
          <span className="percentage-info">{data.wrongpercentage}%</span>
        </div>
      </div>
    </div>
  ));

  return (
    <div className="assignment-statistics-container">
      <div className="top-container">
        <span className="assignment-counter">Toets is {this.state.totalsubmits}x gemaakt</span>
        <span className="result-percentage">Resultaat: {this.state.result}%</span>
        <select className="person-selectbox">
          <option value="person1">Persoon 1</option>
          <option value="person2">Persoon 2</option>
          <option value="person3">Persoon 3</option>
        </select>
      </div>
      <div className="statistics-container">
        <p className="subject-title">{this.state.subjectname}</p>
        {renderQuestions}
      </div>
    </div>
  );
};

export default AssignmentStatistics;