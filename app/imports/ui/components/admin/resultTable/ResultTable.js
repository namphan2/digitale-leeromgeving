import React from 'react';
import { Link } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import _ from 'lodash';

import assignments from '../../../../api/collections/assignments';

import './style.scss';

const ResultTable = (props) => {
  const sortArray = _.orderBy(
    props.assignments,
    [props.sort.orderBy],
    [props.sort.order === 1 ? 'asc' : 'desc'],
  );

  const filteredItems = props.sort.order === 0 ? props.assignments : sortArray;

  const renderTableItems = filteredItems.map(data => (
    <tr key={data._id}>
      <td>{data.name}</td>
      <td>{data.cat}</td>
      <td className="resultPercentage">{data.result}%</td>
      <td>
        {/* <Link to={`/admin/resultaten/${data._id}`}>link</Link> */}
        {/* NO ID YET FOR STATISTICS ASSIGNMENT RESULT */}

        <Link to="/admin/resultaten/opdracht/:id">link</Link>
      </td>
    </tr>
  ));

  return (
    <table className="result-table">
      <thead>
        <tr>
          <th>Naam</th>
          <th>Categorie/Vak</th>
          <th>Resultaat</th>
          <th>Bekijken</th>
        </tr>
      </thead>
      <tbody>{renderTableItems}</tbody>
    </table>
  );
};

export default withTracker(() => {
  Meteor.subscribe('assignments.get');

  return {
    assignments: assignments.find({}).fetch({}),
  };
})(ResultTable);
