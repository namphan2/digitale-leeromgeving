import { Meteor } from 'meteor/meteor';
import assignments from '../imports/api/collections/assignments';
import courses from '../imports/api/collections/courses';
import images from '../imports/api/collections/images';

Meteor.startup(() => {
  Meteor.publish('images.getAll', () => images.find());
  Meteor.publish('assignments.get', () => assignments.find());
  Meteor.publish('courses.get', () => courses.find());
});
