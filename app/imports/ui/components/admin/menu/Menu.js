import React from 'react';
import { Link } from 'react-router-dom';

import './menu.scss';

const Menu = () => (
  <aside className="admin-sidebar">
    <div className="logo-container">
      <div className="logo" />
    </div>
    <div className="menu">
      <span className="menu-title">Menu</span>
      <nav className="main-menu">
        <ul>
          <li className="menu-item">
            <Link to="/admin">Dashboard</Link>
          </li>
          <li className="menu-item">
            <Link to="/admin/cursussen/">Cursussen</Link>
          </li>
          <li className="menu-item">
            <Link to="/admin/opdrachten">Opdrachten</Link>
          </li>
          <li className="menu-item">
            <Link to="/admin/resultaten">Resultaten</Link>
          </li>
          <li className="menu-item">
            <Link to="/admin/groepen">Klassen / groepen</Link>
          </li>
          <li className="menu-item">
            <Link to="/admin/instellingen">Instellingen</Link>
          </li>
        </ul>
      </nav>
    </div>
  </aside>
);

export default Menu;
