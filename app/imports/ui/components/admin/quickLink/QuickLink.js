import React from 'react';

import './style.scss';

const QuickLink = props => (
  <div className="quick-link">
    <div className="icon-container">
      <div className="icon" />
    </div>
    <div className="text-container">
      <span>{props.number}</span>
      <span>{props.text}</span>
    </div>
  </div>
);

export default QuickLink;
