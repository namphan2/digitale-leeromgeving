import React from 'react';

import './pageContainer.css';

const PageContainer = props => <div className="admin-page-container">{props.children}</div>;

export default PageContainer;
