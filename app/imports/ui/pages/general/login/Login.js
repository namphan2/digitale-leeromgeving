import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';

import validate from 'validate.js';
import constraints from '../../../../util/validate/login/constraints';

import './login.scss';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      alert: {
        message: '',
        type: toast.TYPE.INFO,
        status: false,
      },
    };
    this.inputHandler = this.inputHandler.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.alert !== this.state.alert && this.state.alert.status !== false) {
      console.log(this.state.alert);
      const { message, type } = this.state.alert;
      toast(message, { type });
    }
  }

  onFormSubmit(e) {
    e.preventDefault();

    const { email, password } = this.state;

    const formError = this.validateFields();

    if (!formError) {
      Meteor.loginWithPassword(email, password, (error) => {
        if (error) {
          // console.log("asdasd");
          this.setState({
            alert: {
              message: error.reason,
              type: toast.TYPE.ERROR,
              status: true,
            },
          });
        } else {
          this.setState({
            alert: {
              message: '',
              type: toast.TYPE.ERROR,
              status: false,
            },
          });
          const user = Meteor.user();
          if (user.profile.isAdmin) {
            this.props.history.push('/admin');
          } else {
            this.props.history.push('/student');
          }
        }
      });
    }
  }

  inputHandler(event) {
    const inputname = event.target.name;
    const inputvalue = event.target.value;

    this.setState({
      [inputname]: inputvalue,
    });
  }

  validateFields() {
    validate.options = {
      fullMessages: false,
    };

    const { email, password } = this.state;

    const validateResult = validate(
      {
        email,
        password,
      },
      constraints,
    );

    if (validateResult) {
      const message = validateResult[Object.keys(validateResult)[0]][0];

      this.setState({
        alert: {
          message,
          type: toast.TYPE.ERROR,
          status: true,
        },
      });

      return true;
    }

    this.setState({
      alert: {
        message: '',
        type: toast.TYPE.ERROR,
        status: true,
      },
    });

    return false;
  }

  render() {
    return (
      <div>
        <div className="container-container">
          <h1 className="login-header">Inloggen</h1>
          <div className="login-container">
            <p className="login-text">E-mail address: </p>

            <input
              type="email"
              className="login-input"
              id="emailInput"
              name="email"
              value={this.state.email}
              onChange={this.inputHandler}
            />

            <p className="login-text">Password: </p>
            <input
              type="password"
              className="login-input"
              id="passwordInput"
              name="password"
              value={this.state.password}
              onChange={this.inputHandler}
            />

            <input
              type="submit"
              onClick={this.onFormSubmit}
              className="login-button"
              value="Inloggen"
            />

            <Link to="/register" className="link-forget-password">
              Wachtwoord vergeten?
            </Link>
          </div>
        </div>
        <div className="container-container-register">
          <h1 className="register-header">Nieuw?</h1>
          <div className="register-container">
            <p>Heb je nog geen account? Klik op de link hier beneden om je te registreren.</p>
            <form method="get" action="/register">
              <input type="submit" className="register-button" value="Registreren" />
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
