import React from 'react';
import { Link } from 'react-router-dom';

import './style.scss';

const DashboardTable = (props) => {
  const renderDifficultyDots = (difficulty) => {
    const dots = [];

    for (let i = 0; i < difficulty; i += 1) {
      dots.push(<div key={i} className="difficulty-dots" />);
    }

    return dots;
  };

  const { dataList } = props;

  const renderTableItems = dataList.map(data => (
    <tr key={data._id}>
      <td>{data.name}</td>
      <td>{data.cat}</td>
      <td>{renderDifficultyDots(data.niveau)}</td>
      <td>{`${data.result}%`}</td>
      <td>
        <Link to={`/admin/opdrachten/${data._id}`}>link</Link>
      </td>
    </tr>
  ));

  return (
    <table className="dashboard-table">
      <thead>
        <tr>
          <th>Naam</th>
          <th>Vak</th>
          <th>Niveau</th>
          <th>Resultaat</th>
          <th>Bekijk</th>
        </tr>
      </thead>
      <tbody>{renderTableItems}</tbody>
    </table>
  );
};

export default DashboardTable;
