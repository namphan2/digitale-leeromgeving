import React, { Component } from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import { Link } from 'react-router-dom';

import assignments from '../../../../api/collections/assignments';
import DashboardTable from '../../../components/admin/dashboardTable/DashboardTable';
import './Style.scss';

class AssignmentOverview extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.updateTitleType(true);
    this.props.updateTitle('Opdrachten');
  }

  render() {
    return (
      <div className="assignment-overview-container">
        <div className="top-container">
          <Link to="/admin/opdrachten/toevoegen">
            <button className="new-assignment-btn">Nieuwe opdracht</button>
          </Link>
          <button>Sorteren</button>
          <span className="total-selected-assignments">0 Geselecteerd</span>
        </div>
        <DashboardTable dataList={this.props.assignments} />
      </div>
    );
  }
}
export default withTracker(() => {
  Meteor.subscribe('assignments.get');

  return {
    assignments: assignments.find({}).fetch(),
  };
})(AssignmentOverview);
