import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { Link } from 'react-router-dom';

import './Style.scss';

class Questions extends Component {
  constructor(props) {
    super(props);

    this.state = {
      testdata: [
        {
          questionN: 1,
          question: 'wat is een priemgetal?',
          answer1: '32',
          answer2: '32',
          answer3: '32',
          correct: '21',
        },
        {
          questionN: 2,
          question: 'wat is een priemgetal?',
          answer1: '32',
          answer2: '32',
          answer3: '32',
          correct: '21',
        },
        {
          questionN: 3,
          question: 'wat is een priemgetal?',
          answer1: '32',
          answer2: '32',
          answer3: '32',
          correct: '122',
        },
        {
          questionN: 4,
          question: 'wat is een priemgetal?',
          answer1: '32',
          answer2: '32',
          answer3: '32',
          correct: '121',
        },
      ],

      currentQuestion: 1,

    };
  }

  componentDidMount() {
    const elements = document.getElementsByClassName('student-area');
    elements[0].style.backgroundColor = '#ffffff';
  }

  getAmountOfQuestions() {
    return this.state.testdata.length;
  }


  render() {
    return (
      <div className="question-container">
        <Link to="/student">
          <div className="back-button">
            <h1 className="stop-button"> &lt; Stop</h1>
          </div>
        </Link>

        <h1 className="test-subject">Wiskunde</h1>
        <h1 className="question-number">Vraag: {this.state.currentQuestion}/{this.getAmountOfQuestions()}</h1>


        <div className="q-and-a-container">
          <h2 className="question-text">Welk getal is Geen priemgetal?</h2>

          <ul className="answers">
            <li>A  31</li>
            <li>B  43</li>
            <li>C  69</li>

          </ul>
        </div>

        <div className="progress">
          <span>
            <ul className="list">
              <li className="current-square">1</li>
              <li className="square">2</li>
              <li className="square">3</li>
              <li className="square">4</li>
              <li className="square">5</li>
              <li className="square">6</li>
              <li className="square">7</li>
              <li className="square">8</li>
              <li className="square">9</li>
              <li className="square">10</li>
            </ul>
          </span>
        </div>


      </div>

    );
  }
}


export default Questions;
